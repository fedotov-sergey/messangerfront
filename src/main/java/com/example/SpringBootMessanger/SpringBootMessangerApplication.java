package com.example.SpringBootMessanger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMessangerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMessangerApplication.class, args);
	}

}
